#include <bits/stdc++.h>
#include "sum.h"

using ll = long long;
using ld = long double;
using namespace std;


int main() {
#ifdef PAUNSVOKNO
	freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);
#endif
	ios_base::sync_with_stdio(false); cout.setf(ios::fixed); cout.precision(20);
    int a, b;
    cin >> a >> b;
    cout << sum(a, b) << endl;
    return 0;
}